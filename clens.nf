#!/usr/bin/env nextflow

// QC
include { alns_to_sample_swap_check } from '../qc/qc.nf'
include { lenstools_get_n_count_and_freq } from '../lenstools/lenstools.nf'
include { lenstools_get_fda_stats } from '../lenstools/lenstools.nf'

// Preprocessing and alignment
// Short read
include { manifest_to_alns as manifest_to_sr_dna_alns } from '../alignment/alignment.nf'
include { manifest_to_alns as manifest_to_sr_rna_alns } from '../alignment/alignment.nf'

// Alignment sanitization
include { alns_to_procd_alns as alns_to_dna_procd_alns } from '../alignment/alignment.nf'
include { alns_to_procd_alns as alns_to_rna_procd_alns } from '../alignment/alignment.nf'

// Germline variants
include { alns_to_germ_vars } from '../germline/germline.nf'
include { germ_vars_to_filtd_germ_vars } from '../germline/germline.nf'

// Somatic variants
include { alns_to_som_vars } from '../somatic/somatic.nf'
include { som_vars_to_filtd_som_vars } from '../somatic/somatic.nf'
include { som_vars_to_normd_som_vars } from '../somatic/somatic.nf'
include { som_vars_to_isecd_som_vars } from '../somatic/somatic.nf'
include { som_vars_to_union_som_vars } from '../somatic/somatic.nf'

// Phasing of variants
include { make_phased_tumor_vars } from '../seq_variation/seq_variation.nf'
include { make_phased_germline_vars } from '../seq_variation/seq_variation.nf'

// MHC calling
include { user_provided_alleles_to_netmhcpan_alleles } from '../immuno/immuno.nf'
include { extract_alleles_from_manifest } from '../immuno/immuno.nf'
include { procd_fqs_to_mhc_alleles } from '../immuno/immuno.nf'

// Creating tumor VCF (germline + somatic variants)
include { bcftools_index } from '../bcftools/bcftools.nf'
include { bcftools_index_somatic } from '../bcftools/bcftools.nf'
include { germ_and_som_vars_to_tumor_vars } from '../seq_variation/seq_variation.nf'

// Misc
include { htslib_bgzip } from '../htslib/htslib.nf'
include { htslib_bgzip_somatic } from '../htslib/htslib.nf'
include { htslib_bgzip_somatic as htslib_bgzip_somatic_isec } from '../htslib/htslib.nf'
include { snpeff_ann } from '../snpeff/snpeff.nf'
include { snpsift_filter as snpsift_filter_snvs } from '../snpeff/snpeff.nf'
include { snpsift_filter as snpsift_filter_indels} from '../snpeff/snpeff.nf'

// Transcript quantification
include { alns_to_transcript_counts } from '../rna_quant/rna_quant.nf'

// Sorting and indexing transcriptome alignment
include { samtools_sort as samtools_sort_txome } from  '../samtools/samtools.nf'
include { samtools_index as samtools_index_txome } from  '../samtools/samtools.nf'

// Indexing RNA (genome) alignment
include { samtools_index as samtools_index_rna } from  '../samtools/samtools.nf'

// Indexing DNA alignment
include { samtools_index as samtools_index_dna } from  '../samtools/samtools.nf'

// CNAs
include { alns_to_cnas } from '../onco/onco.nf'

// Tumor purity
//include { quants_to_tumor_purities } from '../onco/onco.nf'
include { alns_to_tumor_purities } from '../onco/onco.nf'

// Cancer Cell Fraction (Clonality)
include { lenstools_calculate_ccf } from '../lenstools/lenstools.nf'

// Neoantigen peptides generation
include { som_vars_to_neos as snvs_to_neos} from '../neos/neos.nf'
include { som_vars_to_neos as indels_to_neos} from '../neos/neos.nf'

// Join peptide fastas
include { combine_peptide_fastas } from '../immuno/immuno.nf'

// Aggregate pMHC summaries
include { aggregate_pmhc_summaries } from '../immuno/immuno.nf'

// Annotate pMHC summaries
include { lenstools_annotate_pmhcs } from '../lenstools/lenstools.nf'

// Filter out self pepitdes
include { lenstools_filter_mutant_peptides } from '../lenstools/lenstools.nf'

// pMHC summarization
include { peps_and_alleles_to_antigen_stats } from '../immuno/immuno.nf'

// Quantify pMHC CDS abunance
include { lenstools_get_snv_peptide_read_count } from '../lenstools/lenstools.nf'
include { lenstools_get_indel_peptide_read_count } from '../lenstools/lenstools.nf'
include { lenstools_combine_read_counts } from '../lenstools/lenstools.nf'

// Agretopicity
include { calculate_agretopicity } from '../immuno/immuno.nf'

// Report annotations
include { lenstools_add_generic_annotation as annotate_ccfs } from '../lenstools/lenstools.nf'

// Visualization
include { samtools_faidx } from '../samtools/samtools.nf'
include { lenstools_make_lens_bed } from '../lenstools/lenstools.nf'
include { bam_subsetter } from '../samtools/samtools.nf'
include { igv_reports } from '../igv_reports/igv_reports.nf'

// Make LENS report
include { lenstools_make_lens_report } from '../lenstools/lenstools.nf'
include { lenstools_prioritize_peptides } from '../lenstools/lenstools.nf'

// QC
include { samtools_get_mapped_read_counts } from '../samtools/samtools.nf'
include { samtools_get_targeted_cov_stats } from '../samtools/samtools.nf'
include { samtools_stats } from '../samtools/samtools.nf'

workflow manifest_to_clens {
// require:
//   MANIFEST
  take:
    manifest
  main:
    params.dummy_file = "${params.ref_dir}/dummy_file"


// FASTQS PROCESSING AND ALIGNMENTS
  // Short-read DNA alignments
  manifest_to_sr_dna_alns(
    manifest.filter{ it[4] =~ /^dna|^DNA|^wes|^WES|^wxs|^WXS|^WGS|^wgs/ },
    params.clens$alignment$manifest_to_sr_dna_alns$fq_trim_tool,
    params.clens$alignment$manifest_to_sr_dna_alns$fq_trim_tool_parameters,
    params.clens$alignment$manifest_to_sr_dna_alns$aln_tool,
    params.clens$alignment$manifest_to_sr_dna_alns$aln_tool_parameters,
    params.clens$alignment$manifest_to_sr_dna_alns$aln_ref,
    params.clens$alignment$manifest_to_sr_dna_alns$gtf,
    '')

  // Short-read RNA alignments
  manifest_to_sr_rna_alns(
    manifest.filter{ it[4] =~ /^RNA/ },
    params.clens$alignment$manifest_to_sr_rna_alns$fq_trim_tool,
    params.clens$alignment$manifest_to_sr_rna_alns$fq_trim_tool_parameters,
    params.clens$alignment$manifest_to_sr_rna_alns$aln_tool,
    params.clens$alignment$manifest_to_sr_rna_alns$aln_tool_parameters,
    params.clens$alignment$manifest_to_sr_rna_alns$aln_ref,
    params.clens$alignment$manifest_to_sr_rna_alns$gtf,
    '')

// ALIGNMENT SANITIZATION
  alns_to_dna_procd_alns(
    manifest_to_sr_dna_alns.out.alns,
    '',
    '',
    params.clens$alignment$alns_to_dna_procd_alns$aln_ref,
    params.clens$alignment$alns_to_dna_procd_alns$bed,
    params.clens$alignment$alns_to_dna_procd_alns$gtf,
    params.clens$alignment$alns_to_dna_procd_alns$dup_marker_tool,
    params.clens$alignment$alns_to_dna_procd_alns$dup_marker_tool_parameters,
    params.clens$alignment$alns_to_dna_procd_alns$base_recalibrator_tool,
    params.clens$alignment$alns_to_dna_procd_alns$base_recalibrator_tool_parameters,
    params.clens$alignment$alns_to_dna_procd_alns$indel_realign_tool,
    params.clens$alignment$alns_to_dna_procd_alns$indel_realign_tool_parameters,
    params.clens$alignment$alns_to_dna_procd_alns$known_sites_ref,
    manifest.filter{ it[4] =~ /dna|DNA|wes|WES|wxs|WXS|WGS|wgs/ })

// MHC TYPING
  pats_missing_alleles = Channel.empty()
  pats_with_alleles = Channel.empty()
  pats_with_standard_alleles = Channel.empty()
  manifest.filter{ it[6] =~ /NA|Null|null/ }.set{ pats_missing_alleles }
  manifest.filter{ !(it[6] =~ /NA|Null|null/) }.set{ pats_with_alleles }
  user_provided_alleles_to_netmhcpan_alleles(
      pats_with_alleles.filter{ it[4] =~ /RNA/ }.map{ [it[0], it[1], it[2], it[6]] })
  user_provided_alleles_to_netmhcpan_alleles.out.netmhcpan_alleles
    .set{ pats_with_standard_alleles }
  procd_fqs_to_mhc_alleles(
    manifest_to_sr_rna_alns.out.procd_fqs.filter{ it[1] =~ 'ar-' },
    params.clens$immuno$procd_fqs_to_mhc_alleles$aln_tool,
    params.clens$immuno$procd_fqs_to_mhc_alleles$aln_tool_parameters,
    params.clens$immuno$procd_fqs_to_mhc_alleles$aln_ref,
    params.clens$immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool,
    params.clens$immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool_parameters)
  procd_fqs_to_mhc_alleles.out.alleles
    .concat(pats_with_standard_alleles)
    .set{ all_pat_mhc_alleles }

// SOMATIC VARIANTS
   alns_to_som_vars(
     alns_to_dna_procd_alns.out.procd_bams.filter{ it[1] =~ /nd-|ad-/ },
     params.clens$somatic$alns_to_som_vars$som_var_caller,
     params.clens$somatic$alns_to_som_vars$som_var_caller_parameters,
     params.clens$somatic$alns_to_som_vars$som_var_caller_suffix,
     params.clens$somatic$alns_to_som_vars$aln_ref,
     params.clens$somatic$alns_to_som_vars$bed,
     params.clens$somatic$alns_to_som_vars$som_var_pon_vcf,
     params.clens$somatic$alns_to_som_vars$som_var_af_vcf,
     params.clens$somatic$alns_to_som_vars$known_sites_ref,
     params.clens$somatic$alns_to_som_vars$species,
     manifest.filter{ it[4] =~ /dna|DNA|wes|WES|wxs|WXS|WGS|wgs/ })

// SOMATIC SANTIZATION
  // Somatic variant filtering
    som_vars_to_filtd_som_vars(
      alns_to_som_vars.out.som_vars,
      params.clens$somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool,
      params.clens$somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool_parameters)

  // Somatic variant indel normalizing
    som_vars_to_normd_som_vars(
      som_vars_to_filtd_som_vars.out.filtd_som_vars,
      params.clens$somatic$som_vars_to_normd_som_vars$vcf_norming_tool,
      params.clens$somatic$som_vars_to_normd_som_vars$vcf_norming_tool_parameters,
      params.clens$somatic$som_vars_to_normd_som_vars$aln_ref)

    htslib_bgzip_somatic(
      som_vars_to_normd_som_vars.out.normd_som_vars)

    joint_vars = Channel.empty()

    if (params.clens$somatic$combine_strategy =~ /intersect/) {
      // Somatic variant intersectioning
        som_vars_to_isecd_som_vars(
          htslib_bgzip_somatic.out.bgzip_files,
          params.clens$somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool,
          params.clens$somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool_parameters)
        som_vars_to_isecd_som_vars.out.isecd_som_vars
          .set{ joint_vars }
     }
     if (params.clens$somatic$combine_strategy =~ /merge|union/) {
        som_vars_to_union_som_vars(
          htslib_bgzip_somatic.out.bgzip_files,
          params.clens$somatic$som_vars_to_union_som_vars$vcf_merging_tool,
          params.clens$somatic$som_vars_to_union_som_vars$vcf_merging_tool_parameters)
        som_vars_to_union_som_vars.out.union_som_vars
          .set{ joint_vars }
     }

    // Annotate somatic variants
    // This should be in an annot workflow that allows different tools.
    snpeff_ann(
      joint_vars,
      params.clens$snpeff$annot_tool_ref)


    // Filter somatic SNVs
    snpsift_filter_snvs(
      snpeff_ann.out.annotd_vcfs,
      params.clens$snpsift_filter_snvs$snpsift_snv_filter_parameters,
      "sfilt.snvs")

    // Filter somatic InDels
    snpsift_filter_indels(
      snpeff_ann.out.annotd_vcfs,
      params.clens$snpsift_filter_indels$snpsift_indel_filter_parameters,
      "sfilt.indels")

// GERMLINE VARIANTS
   alns_to_germ_vars(
     alns_to_dna_procd_alns.out.procd_bams.filter{ it[1] =~ /nd-/ },
     params.clens$germline$sr_alns_to_germ_vars$germ_var_caller,
     params.clens$germline$sr_alns_to_germ_vars$germ_var_caller_parameters,
     params.clens$germline$sr_alns_to_germ_vars$germ_var_caller_suffix,
     params.clens$germline$sr_alns_to_germ_vars$aln_ref,
     params.clens$germline$sr_alns_to_germ_vars$bed)

// GERMLINE SANITIZATION
  // Germline variant filtering
    germ_vars_to_filtd_germ_vars(
      alns_to_germ_vars.out.germ_vars,
      params.clens$germline$germ_vars_to_filtd_germ_vars$vcf_filtering_tool,
      params.clens$germline$germ_vars_to_filtd_germ_vars$vcf_filtering_tool_parameters)

// MAKE TUMOR VARIANTS
// Combine somatic and germline variants and phase to create "tumor"
// variants (targetable variants coupled with phased, neighboring germline
// and somatic variants.)

  htslib_bgzip(
    germ_vars_to_filtd_germ_vars.out.filtd_germ_vars)
  bcftools_index(
    htslib_bgzip.out.bgzip_files,
    params.clens$bcftools$bcftools_index$bcftools_index_parameters)
  htslib_bgzip_somatic_isec(
    snpeff_ann.out.annotd_vcfs)
  bcftools_index_somatic(
    htslib_bgzip_somatic_isec.out.bgzip_files,
    params.clens$bcftools$bcftools_index_somatic$bcftools_index_somatic_parameters)
  germ_and_som_vars_to_tumor_vars(
    bcftools_index.out.vcfs_w_csis,
    bcftools_index_somatic.out.vcfs_w_csis,
    params.clens$seq_variation$germ_and_som_vars_to_tumor_vars$vcf_merge_tool,
    params.clens$seq_variation$germ_and_som_vars_to_tumor_vars$vcf_merge_tool_parameters)

// RNA ALIGNMENT SANITIZATION
// Utilizes somatic and germline indel calls
  // Sanitizing RNA alignments
  alns_to_rna_procd_alns(
    manifest_to_sr_rna_alns.out.alns.filter{ it[1] =~ /ar-/ },
    manifest_to_sr_rna_alns.out.junctions.filter{ it[1] =~ /ar-/},
    germ_and_som_vars_to_tumor_vars.out.tumor_vars,
    params.clens$alignment$alns_to_rna_procd_alns$aln_ref,
    params.clens$alignment$alns_to_rna_procd_alns$bed,
    params.clens$alignment$alns_to_rna_procd_alns$gtf,
    params.clens$alignment$alns_to_rna_procd_alns$dup_marker_tool,
    params.clens$alignment$alns_to_rna_procd_alns$dup_marker_tool_parameters,
    params.clens$alignment$alns_to_rna_procd_alns$base_recalibrator_tool,
    params.clens$alignment$alns_to_rna_procd_alns$base_recalibrator_tool_parameters,
    params.clens$alignment$alns_to_rna_procd_alns$indel_realign_tool,
    params.clens$alignment$alns_to_rna_procd_alns$indel_realign_tool_parameters,
    params.clens$alignment$alns_to_rna_procd_alns$known_sites_ref,
    manifest.filter{ it[4] =~ /rna|RNA|RNA-Seq|RNA-seq/ })

// VARIANT PHASING
    // Phasing of tumor variants
    make_phased_tumor_vars(
      germ_and_som_vars_to_tumor_vars.out.tumor_vars_and_idxs,
      alns_to_dna_procd_alns.out.procd_bams_and_bais,
      alns_to_rna_procd_alns.out.procd_bams_and_bais,
      params.clens$seq_variation$make_phased_tumor_vars$aln_ref,
      params.clens$seq_variation$make_phased_tumor_vars$gtf,
      params.clens$seq_variation$make_phased_tumor_vars$species,
      params.clens$seq_variation$make_phased_tumor_vars$var_phaser_tool,
      params.clens$seq_variation$make_phased_tumor_vars$var_phaser_tool_parameters)

    // Phasing of germline variants
    make_phased_germline_vars(
      bcftools_index.out.vcfs_w_csis,
      alns_to_dna_procd_alns.out.procd_bams_and_bais,
      params.clens$seq_variation$make_phased_germline_vars$aln_ref,
      params.clens$seq_variation$make_phased_germline_vars$gtf,
      params.clens$seq_variation$make_phased_germline_vars$var_phaser_tool,
      params.clens$seq_variation$make_phased_germline_vars$var_phaser_tool_parameters)

// TRANSCRIPT COUNTS
  alns_to_transcript_counts(
    manifest_to_sr_rna_alns.out.alt_alns,
    params.clens$rna_quant$alns_to_transcript_counts$rna_ref,
    params.clens$rna_quant$alns_to_transcript_counts$gtf,
    params.clens$rna_quant$alns_to_transcript_counts$tx_quant_tool,
    params.clens$rna_quant$alns_to_transcript_counts$tx_quant_tool_parameters)

  // Sorting and indexing txome BAM for downstream application
  samtools_sort_txome(
    manifest_to_sr_rna_alns.out.alt_alns.filter{ it[1] =~ 'ar-' },
    '')
  samtools_index_txome(
    samtools_sort_txome.out.bams,
    '')

  // Indexing RNA BAM for downstream application
  samtools_index_rna(
    manifest_to_sr_rna_alns.out.alns.filter{ it[1] =~ 'ar-' },
    '')

// SNVs to peptides
  snvs_to_neos(
    snpsift_filter_snvs.out.filtd_vcfs,
    make_phased_tumor_vars.out.phased_vcfs,
    make_phased_germline_vars.out.phased_vcfs,
    alns_to_transcript_counts.out.quants,
    params.clens$neos$snvs_to_neos$gtf,
    params.clens$neos$snvs_to_neos$dna_ref,
    params.clens$neos$snvs_to_neos$pep_ref,
    params.clens$neos$snvs_to_neos$som_var_type,
    params.clens$neos$snvs_to_neos$lenstools_filter_expressed_variants_parameters,
    params.clens$neos$snvs_to_neos$bcftools_index_phased_germline_parameters,
    params.clens$neos$snvs_to_neos$bcftools_index_phased_tumor_parameters,
    params.clens$neos$snvs_to_neos$lenstools_get_expressed_transcripts_bed_parameters,
    params.clens$neos$snvs_to_neos$samtools_faidx_fetch_somatic_folder_parameters)

// InDels to peptides
  indels_to_neos(
    snpsift_filter_indels.out.filtd_vcfs,
    make_phased_tumor_vars.out.phased_vcfs,
    make_phased_germline_vars.out.phased_vcfs,
    alns_to_transcript_counts.out.quants,
    params.clens$neos$indels_to_neos$gtf,
    params.clens$neos$indels_to_neos$dna_ref,
    params.clens$neos$indels_to_neos$pep_ref,
    params.clens$neos$indels_to_neos$som_var_type,
    params.clens$neos$indels_to_neos$lenstools_filter_expressed_variants_parameters,
    params.clens$neos$indels_to_neos$bcftools_index_phased_germline_parameters,
    params.clens$neos$indels_to_neos$bcftools_index_phased_tumor_parameters,
    params.clens$neos$indels_to_neos$lenstools_get_expressed_transcripts_bed_parameters,
    params.clens$neos$indels_to_neos$samtools_faidx_fetch_somatic_folder_parameters)

// Combining peptides from all antigen sources
    snvs_to_neos.out.som_var_c1_peptides.map{ [it[0], it[3], it[4]] }
    .join(indels_to_neos.out.som_var_c1_peptides.map{ [it[0], it[3], it[4]] }, by: [0,1], remainder: true)
    .map{ [*it.asList().minus(null)] }
    .map{ [it[0], it[1], it[2..-1]] }
    .set{ init_joint_peptides }
  combine_peptide_fastas(
    init_joint_peptides)
    .map{ [it[0], 'NA', it[1], it[2]] }
    .set{ combined_peptides }

  // Summarize pMHCs
  peps_and_alleles_to_antigen_stats(
    combined_peptides,
    all_pat_mhc_alleles,
    params.clens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool,
    params.clens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool_parameters,
    params.clens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir,
    params.clens$immuno$peps_and_alleles_to_antigen_stats$species,
    params.clens$immuno$peps_and_alleles_to_antigen_stats$peptide_lengths)

    aggregate_pmhc_summaries(
      peps_and_alleles_to_antigen_stats.out.all_antigen_outputs)

    aggregate_pmhc_summaries.out.pmhc_aggs_tsv
      .join(combine_peptide_fastas.out.peptide_fastas, by: [0,1])
      .set{ pmhc_summs_and_fastas }

    lenstools_annotate_pmhcs(
      pmhc_summs_and_fastas,
      params.clens$lenstools$lenstools_annotate_pmhcs$binding_affinity_threshold)

    lenstools_filter_mutant_peptides(
      lenstools_annotate_pmhcs.out.high_aff_annotated_pmhcs,
      params.clens$lenstools$lenstools_filter_mutant_peptides)

    snv_read_counts = Channel.empty()
    indel_read_counts = Channel.empty()

    // Get SNV RNA read support
    snvs_to_neos.out.som_var_c1_nts.map{ [it[0], it[3], it[4]] }
      .join(lenstools_filter_mutant_peptides.out.pep_filtered_pmhcs, by: [0, 1])
      .join(samtools_index_txome.out.bams_and_bais, by: 0)
      .map{ [it[0], it[1], it[6], it[7], it[2], it[3]] }
      .set{ snv_peptide_read_count_inputs }
    lenstools_get_snv_peptide_read_count(
      snv_peptide_read_count_inputs,
      params.clens$lenstools$lenstools_get_snv_peptide_read_count$gtf)
    lenstools_get_snv_peptide_read_count.out.peptide_read_counts
      .set{ snv_read_counts }

    // Get InDel RNA read support
    indels_to_neos.out.som_var_c1_nts.map{ [it[0], it[3], it[4]] }
      .join(lenstools_filter_mutant_peptides.out.pep_filtered_pmhcs, by: [0, 1])
      .join(samtools_index_rna.out.bams_and_bais, by: 0)
      .map{ [it[0], it[1], it[6], it[7], it[2], it[3]] }
      .set{ indel_peptide_read_count_inputs }
    lenstools_get_indel_peptide_read_count(
      indel_peptide_read_count_inputs,
      params.clens$lenstools$lenstools_get_indel_peptide_read_count$gtf)
    lenstools_get_indel_peptide_read_count.out.peptide_read_counts
      .set{ indel_read_counts }

    Channel.empty()
     .concat(snv_read_counts)
     .concat(indel_read_counts)
     .groupTuple(by: [0, 1])
     .set{ pan_source_counts_by_pat }

   lenstools_combine_read_counts(
     pan_source_counts_by_pat)

    // Agretopicity
    calculate_agretopicity(
      lenstools_combine_read_counts.out.pmhcs_with_read_counts,
      params.clens$immuno$calculate_agretopicity$blastp_db_dir,
      params.clens$immuno$peps_and_alleles_to_antigen_stats$species,
      all_pat_mhc_alleles,
      params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool,
      params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool_parameters,
      params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir,
      params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$peptide_lengths)

// Tumor purity
  alns_to_tumor_purities(
   alns_to_dna_procd_alns.out.procd_bams.filter{ it[1] =~ /nd-|ad-/ },
   params.clens$onco$alns_to_tumor_purities$tx_quant_tool,
   params.clens$onco$alns_to_tumor_purities$tx_quant_tool_parameters,
   params.clens$onco$alns_to_tumor_purities$tumor_purities_tool,
   params.clens$onco$alns_to_tumor_purities$tumor_purities_tool_parameters,
   params.clens$onco$alns_to_tumor_purities$aln_ref,
   params.clens$onco$alns_to_tumor_purities$gtf,
   params.clens$onco$alns_to_tumor_purities$bed,
   manifest)

// CNA and cancer cell fraction
  alns_to_cnas(
    alns_to_dna_procd_alns.out.procd_bams,
    make_phased_tumor_vars.out.phased_vcfs,
    params.clens$onco$alns_to_cnas$cna_tool,
    params.clens$onco$alns_to_cnas$cna_tool_refs,
    params.clens$onco$alns_to_cnas$cna_tool_parameters,
    params.clens$onco$alns_to_cnas$aln_ref,
    params.clens$onco$alns_to_cnas$bed,
    params.clens$onco$alns_to_cnas$gtf,
    manifest.filter{ it[4] =~ /WES/ })

  snpeff_ann.out.annotd_vcfs.map{ [it[0], it[3], it[4]] }
  .join(alns_to_cnas.out.cnas.map{ [it[0], it[2], it[3]] }, by:[0,1])
  .join(alns_to_tumor_purities.out.tumor_purities.map{ [it[0], it[3], it[4]] }, by:[0,1])
  .set{ calculate_ccf_inputs }


// CCF
  lenstools_calculate_ccf(
    calculate_ccf_inputs)

    calculate_agretopicity.out.pmhcs_with_agretos
      .join(lenstools_calculate_ccf.out.ccfs, by: [0,1])
      .set{ annot_ccfs_input }
    annotate_ccfs(
      annot_ccfs_input,
      '.ccfs',
      '-a variant_pos -b variant -c vaf,totcopynum,multiplicity,ccf')

    // Make antigen-specific IGV files
    lenstools_make_lens_bed(
      lenstools_combine_read_counts.out.pmhcs_with_read_counts)

    samtools_index_dna(
      alns_to_dna_procd_alns.out.procd_bams,
      '')

    samtools_index_dna.out.bams_and_bais.filter{ it[1] =~ 'nd-' }.set{ norm_dna_bams_bais }
    samtools_index_dna.out.bams_and_bais.filter{ it[1] =~ 'ad-' }.set{ tumor_dna_bams_bais }

    norm_dna_bams_bais
      .join(tumor_dna_bams_bais, by: [0, 2])
      .set{ norm_tumor_dna_bams_and_bais }

    norm_tumor_dna_bams_and_bais
      .join(alns_to_rna_procd_alns.out.procd_bams_and_bais
        .filter{ it[1] =~ 'ar-' }
        .map{ [it[0], it[2], it[1], it[3], it[4]] }, by: [0, 1])
      .set{ all_pat_bams }

    all_pat_bams
      .join(lenstools_make_lens_bed.out.lens_bed, by: [0, 1])
      .set{ all_pat_bams_with_beds }

    bam_subsetter(
      all_pat_bams_with_beds)

    samtools_faidx(
      params.clens$alignment$manifest_to_sr_dna_alns$aln_ref,
      '')

    igv_reports(
      bam_subsetter.out.subsetted_bams_w_bed,
      samtools_faidx.out.faidx_file)

    lenstools_prioritize_peptides(
      annotate_ccfs.out.annoted_files,
      params.lens_out_dir)

    lenstools_make_lens_report(
      annotate_ccfs.out.annoted_files,
      params.lens_out_dir)

manifest_to_sr_rna_alns.out.fqs
  .concat(manifest_to_sr_dna_alns.out.fqs)
  .set{ raw_reads }

samtools_index_dna.out.bams_and_bais
  .concat(samtools_index_rna.out.bams_and_bais)
  .set{ qc_bams_and_bais }

alns_to_sample_swap_check(
  qc_bams_and_bais,
  manifest,
  params.clens$qc$bams_to_sample_swap_check$sample_swap_tool,
  params.clens$qc$bams_to_sample_swap_check$sample_swap_tool_dna_ref,
  params.clens$qc$bams_to_sample_swap_check$sample_swap_tool_known_sites,
  params.clens$qc$bams_to_sample_swap_check$sample_swap_tool_parameters)

samtools_stats(
  qc_bams_and_bais.map{ [it[0], it[1], it[2], it[3]] },
  '')

samtools_get_targeted_cov_stats(
  qc_bams_and_bais,
  params.clens$qc$samtools_get_targeted_cov_stats$bed,
  '--ff "UNMAP,SECONDARY,QCFAIL"')

lenstools_get_n_count_and_freq(
  raw_reads)

manifest_to_sr_dna_alns.out.fqs
  .join(manifest_to_sr_dna_alns.out.fq_stats, by: [0, 1, 2])
  .join(samtools_stats.out.bam_stats, by: [0,1,2])
  .join(samtools_get_targeted_cov_stats.out.cov_stats, by: [0,1,2])
  .set{ dna_stats }

manifest_to_sr_rna_alns.out.fqs
  .join(manifest_to_sr_rna_alns.out.fq_stats, by: [0, 1, 2])
  .join(samtools_stats.out.bam_stats, by: [0,1,2])
  .join(samtools_get_targeted_cov_stats.out.cov_stats, by: [0,1,2])
  .set{ rna_stats }

dna_stats
  .concat(rna_stats)
  .set{ aln_stats }

aln_stats
  .join(lenstools_get_n_count_and_freq.out.fq_n_freq, by: [0, 1, 2])
  .set{ aln_and_fq_stats }

lenstools_get_fda_stats(
  aln_and_fq_stats,
  params.clens$neos$snvs_to_neos$dna_ref,
  params.clens$neos$snvs_to_neos$gtf, 
  params.clens$lenstools$lenstools_get_fda_stats$parameters)
}
